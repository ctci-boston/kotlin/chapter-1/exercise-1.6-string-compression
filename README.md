# exercise-1.6-string-compression

Provide a Kotlin solution to determine if a string can be compressed by encoding sequences of characters using the character count. Return the smaller of the original string or the compressed string.