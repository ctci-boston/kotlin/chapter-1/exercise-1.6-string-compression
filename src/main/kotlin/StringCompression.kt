fun getEncodedString(s: String): String {
    class Encoding {
        val result = StringBuilder()
        var currentChar = '\u0000'
        var occurrences = 1
        fun add() { result.append(currentChar, occurrences) }
        fun size(): Int = result.length
        override fun toString(): String = result.toString()
    }
    val encoding = Encoding()
    var i = 0
    fun encode() {
        encoding.currentChar = s[i]
        encoding.occurrences = 1
        while (i + 1 < s.length && s[i + 1] == s[i]) { encoding.occurrences++; i++ }
        encoding.add()
        i++
    }

    if ("""[^a-zA-Z]+""".toRegex().matches(s)) return ""
    while (i < s.length) { encode() }
    return if (encoding.size() < s.length) encoding.toString() else s
}
