import kotlin.test.Test
import kotlin.test.assertEquals

class Test {
    @Test fun `Given an empty string verify no compression`() {
        val s = ""
        val expected = ""
        assertEquals(expected, getEncodedString(s))
    }

    @Test fun `Given a non-empty string verify compression`() {
        val s = "aaabbcddddbbb"
        val expected = "a3b2c1d4b3"
        assertEquals(expected, getEncodedString(s))
    }

    @Test fun `Given a non-empty string verify no compression`() {
        val s = "abcd"
        val expected = "abcd"
        assertEquals(expected, getEncodedString(s))
    }

    @Test fun `Given invalid characters verify empty string`() {
        val s = "1234!@#"
        val expected = ""
        assertEquals(expected, getEncodedString(s))
    }
}
